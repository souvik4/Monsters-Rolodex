// import {Component} from 'react';
// import './App.css';
// import CardList from './components/card-list/card-list.component';
// import SearchBox from './components/search-box/search-box.component';

// class App extends Component {
//   constructor(){
//     super();

//     this.state = {
//       monsters: [],
//       searchField: ''
//     }
//   }

//   componentDidMount(){
//    fetch('https://jsonplaceholder.typicode.com/users')
//    .then(response => response.json())
//    .then((users) => this.setState(() => {
//      return {monsters :users}
//    }
//    )); 
//   }

//   onSearchChange = (event) => {
//       console.log(event.target.value)
//       const searchField = event.target.value.toLocaleLowerCase()
//       this.setState(() => {
//         return {searchField}
//       })
//     }
  

//   render(){

//     const {monsters, searchField} = this.state;
//     const {onSearchChange} = this;

//     const filteredMonsters = monsters.filter((monster) => {
//       return monster.name.toLocaleLowerCase().includes(searchField)
//     })


//     return (

//       <div className="App">
//         <h1 className='app-title'>Monter Rolodex</h1>
//         <SearchBox 
//         className='monster-search-box'
//           onChangeHandler={onSearchChange} 
//           placeholder='search Monsters'
//         />
//         <CardList monsters={filteredMonsters}/>
//       </div>
//     );
//   }
// }

// export default App;

        //functional component

// import {Component} from 'react';
// import './App.css';
// import CardList from './components/card-list/card-list.component';
// import SearchBox from './components/search-box/search-box.component';

// class App extends Component {
//   constructor(){
//     super();

//     this.state = {
//       monsters: [],
//       searchField: ''
//     }
//   }

//   componentDidMount(){
//    fetch('https://jsonplaceholder.typicode.com/users')
//    .then(response => response.json())
//    .then((users) => this.setState(() => {
//      return {monsters :users}
//    }
//    )); 
//   }

//   onSearchChange = (event) => {
//       console.log(event.target.value)
//       const searchField = event.target.value.toLocaleLowerCase()
//       this.setState(() => {
//         return {searchField}
//       })
//     }
  

//   render(){

//     const {monsters, searchField} = this.state;
//     const {onSearchChange} = this;

//     const filteredMonsters = monsters.filter((monster) => {
//       return monster.name.toLocaleLowerCase().includes(searchField)
//     })


//     return (

//       <div className="App">
//         <h1 className='app-title'>Monter Rolodex</h1>
//         <SearchBox 
//         className='monster-search-box'
//           onChangeHandler={onSearchChange} 
//           placeholder='search Monsters'
//         />
//         <CardList monsters={filteredMonsters}/>
//       </div>
//     );
//   }
// }<CardList monsters={filteredMonsters}/>

// export default App;
//59

import CardList from './components/card-list/card-list.component';
import SearchBox from './components/search-box/search-box.component';
import './App.css'
import {useState, useEffect} from 'react'

 const App = () => {

  const [searchField, setSearchField] = useState('');
  const [monsters, setMonsters] = useState([])
  const [filteredMonsters, setFilterMonster] = useState(monsters)

  
  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
   .then(response => response.json())
   .then((users) => setMonsters(users))
  },[])

  useEffect(() => {
    const newFilteredMonsters = monsters.filter((monster) => {
      return monster.name.toLocaleLowerCase().includes(searchField)
    });
    setFilterMonster(newFilteredMonsters);
    console.log('effect is firing')
  },[monsters, searchField])

  const onSearchChange = (event) => {
          const searchFieldString = event.target.value.toLocaleLowerCase()
          setSearchField(searchFieldString);
    };




    return (
      <div className="App">
        <h1 className='app-title'>Monter Rolodex</h1>
        <SearchBox 
            className='monster-search-box'
              onChangeHandler={onSearchChange} 
              placeholder='search Monsters'
          />
        <CardList monsters={filteredMonsters}/>
      </div>
        );
}

export default App;